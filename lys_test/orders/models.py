# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class OrderStatus(models.Model):
    status = models.CharField(max_length=30)
    label = models.CharField(max_length=30)

    def __unicode__(self):
        return self.label


# class OrderSource(models.Model):
#     source = models.CharField(max_length=50)

#     def __unicode__(self):
#         return self.source


# class OrderProcurementStatus(models.Model):
#     status = models.CharField(max_length=20)

#     def __unicode__(self):
#         return self.status


# class OrderSupplier(models.Model):
#     supplier = models.CharField(max_length=50)

#     def __unicode__(self):
#         return self.supplier


class Product(models.Model):
    name = models.CharField(max_length=100)
    url = models.URLField()
    price = models.DecimalField(decimal_places=4, max_digits=10)

    def __unicode__(self):
        return self.name


class Order(models.Model):
    order_id = models.CharField(max_length=50)
    product_name = models.CharField(max_length=100)
    order_status = models.ForeignKey(OrderStatus)
    product = models.ForeignKey(Product)
    cost_price = models.DecimalField(decimal_places=4, max_digits=10)
    # order_date = models.DateField()
    # order_deadline = models.DateField()
    # order_source = models.ForeignKey(OrderSource)
    # product_code = models.CharField(max_length=30)
    # selling_price = models.DecimalField(decimal_places=4, max_digits=10)
    # quantity = models.IntegerField(default=1)
    # customer_name = models.CharField(max_length=100)
    # customer_email = models.EmailField()
    # customer_phone = models.CharField(max_length=15)
    # customer_address = models.TextField()
    # payment_type = models.CharField(max_length=50)
    # procurement_status = models.ForeignKey(OrderProcurementStatus)
    # procurement_date = models.DateField()
    # supplier = models.ForeignKey(OrderSupplier)
    # package_id = models.CharField(max_length=20)
    # consolidation_id = models.CharField(max_length=50)
    # usa_tracking = models.CharField(max_length=100)
    # tracking_number = models.CharField(max_length=30)
    # comments = models.TextField()

    def __unicode__(self):
        return "{0} - {1} - {2}".format(self.order_id, self.product_name, self.cost_price)


# class Admin(models.Model):
#     username = models.CharField(max_length=30)
#     created_at = models.DateField(auto_now_add=True)

#     def __unicode__(self):
#         return self.username


# class EditLog(models.Model):
#     order = models.ForeignKey(Order)
#     user = models.ForeignKey(Admin)
#     change_date = models.DateTimeField()
#     changes = models.TextField()

#     def __unicode__(self):
#         return "; {{Edit:{0}-{1}.Changes:{2}}}".format(self.user, self.change_date, self.changes)



