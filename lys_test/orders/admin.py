# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Product, OrderStatus, Order

admin.site.register(Product)
admin.site.register(OrderStatus)
admin.site.register(Order)
