# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

from .models import Order
from .forms import OrderForm

@login_required
def list_orders(request):
    """
    List all the orders
    """
    response_dict = {}

    count_per_page = 25
    orders = Order.objects.all().select_related('product', 'order_status')
    total_pages = int(orders.count() / count_per_page) + (orders.count() % count_per_page > 0)

    page_number = request.GET.get('page', 1)
    try:
        page_number = int(page_number)
    except:
        page_number = 1
    else:
        if page_number > total_pages:
            page_number = total_pages

    sort_by = request.GET.get('sort_by', None)
    if sort_by == "order_status":
        sort_by = 'order_status__status'
    elif sort_by == "product_url":
        sort_by = 'product__product_url'
    
    start = (page_number - 1) * count_per_page
    end = page_number * count_per_page

    if sort_by:
        paginated_orders = orders.order_by(sort_by)[start:end]
    else:
        paginated_orders = orders[start:end]
    
    response_dict['orders'] = paginated_orders
    response_dict['pagination'] = {
        'current_page': page_number,
        'total_pages': total_pages,
        'next_page': page_number + 1 if page_number < total_pages else 0,
        'previous_page': page_number - 1 if page_number > 0 else 0
    }

    return render(request, 'orders.html', response_dict)

@login_required
def create_order(request):
    """
    Page to create an order
    """
    response_dict = {}

    order_form = OrderForm()

    if request.method == "POST":
        order_form = OrderForm(request.POST)
        if order_form.is_valid():
            order_form.save()
            return HttpResponseRedirect('/orders/list/')

    response_dict['form'] = order_form

    return render(request, 'create_order.html', response_dict)

def index(request):
    """
    Index view just to redirec the user to order list page
    """
    return HttpResponseRedirect("/orders/list/")
