import csv
from decimal import Decimal

from .models import OrderStatus, Order, Product

def insert_data():
    """
    Fill the table with the data in csv file
    :return:
    """
    f = open('extras/Test-Orders_DB.csv', 'rU')
    csv_reader = csv.reader(f)
    to_insert = []
    print "Reading file..."
    for line in csv_reader:
        if line[0].strip().lower() == "id":
            continue
        order_id = line[2].strip()
        product_name = line[6].strip()
        order_status = line[1].strip()
        product_url = line[7].strip()
        cost_price = line[23].strip()
        if cost_price:
            price_in_inr = Decimal(cost_price) * 66
        else:
            price_in_inr = Decimal("0")

        try:
            order_status_obj = OrderStatus.objects.get(status=order_status.lower())
        except:
            order_status_obj = OrderStatus.objects.create(status=order_status.lower(), label=order_status)

        try:
            product = Product.objects.get(url=product_url)
        except Product.DoesNotExist:
            product = Product.objects.create(name=product_name, url=product_url, price=price_in_inr)
        to_insert.append(Order(order_id=order_id, product_name=product_name, order_status=order_status_obj, product=product, cost_price=price_in_inr))
        
    print "Inserting rows"

    Order.objects.bulk_create(to_insert)

    print "Done"
    return